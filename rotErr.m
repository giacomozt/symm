function errs = rotErr(args)
    errs = zeros(size(args));
    
    RTrue = eul2rotm(deg2rad(zeros(1, 3)));
    nTrue = RTrue*[0;1;0];
    for y = 1:size(args, 1)
        for x = 1:size(args, 2)
            arg = args{y, x};
            R = eul2rotm(deg2rad([arg(3), arg(2), arg(1)]));
            n = R*[0;1;0];
            errs(y, x) = abs(acosd(n'*nTrue));
        end
    end
    errs = 1-errs;
end