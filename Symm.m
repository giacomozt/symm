classdef Symm < handle
    properties
        vInfo
        vImPos
        vPxSpac
        vOrig
        vOut
        fullRes
        mask
        maskBounds
        excludeMask
        excludeNs
        excludeCs
        includeMask
        includeNs
        includeCs
        voxelMap
        chunkMap
        chunkDim
        initArgs
        optArgs
        corrs
        minScore
        minLeftVoxels
        minRightVoxels
        minValid
        minLeftVals
        minRightVals
        threshLo
        threshHi
        ERR_METRIC
        TUKEY_THRESH
        TUKEY_DEV
        HUBER_THRESH
        HUBER_DEV
        ALGORITHM
        ANG_BOUND
        ANG_STEP
        TRANS_STEP
        MAX_IT
        
        debugTrace
    end
    
    methods
        function obj = Symm(path)
            fprintf('loading volume... ');
            [V, S, ~] = dicomreadVolume(path);
            obj.vOrig = double(squeeze(V));
            %obj.vOrig = obj.vOrig(:, :, 1:256);
            list = dir(path);
            obj.vInfo = dicominfo([path, '\', list(3).name]);
            obj.vImPos = S.PatientPositions(1, 1:3);
            obj.vPxSpac = S.PixelSpacings(1, 1:2);
            obj.vPxSpac(1, 3) = S.PatientPositions(2, 3)-S.PatientPositions(1, 3);
            obj.initArgs = [0, 0, 0, size(obj.vOrig)/2];
            fprintf('done\n');
            
            fprintf('enumerating voxels... ');
            obj.enum_voxels();
            obj.enum_chunks(3, 3);
            fprintf('done\n');
            
            fprintf('creating (default) mask... ');
            obj.maskBounds =  [zeros(3, 1), size(obj.vOrig)'];%int16([size(obj.vOrig, 1)/4, 3*size(obj.vOrig, 1)/4;
                              %5*size(obj.vOrig, 2)/16, 11*size(obj.vOrig, 2)/16;
                              %1*size(obj.vOrig, 3)/16, 7*size(obj.vOrig, 3)/16;]);
            obj.make_mask();
            obj.excludeMask = true(size(obj.vOrig(:)))';
            obj.includeMask = true(size(obj.vOrig(:)))';
            fprintf('done\n');
            
            fprintf('bone thresholding... ');
            obj.proc_hist();
            fprintf('done\n');
            
            obj.minScore = inf;
            
            if ismember('ERR_METRIC', evalin('base', 'who'))
                obj.ERR_METRIC = evalin('base', 'ERR_METRIC');
            else
                obj.ERR_METRIC = 'ncc';
            end
            if ismember('TUKEY_THRESH', evalin('base', 'who'))
                obj.TUKEY_THRESH = evalin('base', 'TUKEY_THRESH');
            else
                obj.TUKEY_THRESH = 4.685;
            end
            if ismember('TUKEY_DEV', evalin('base', 'who'))
                obj.TUKEY_DEV = evalin('base', 'TUKEY_DEV');
            else
                obj.TUKEY_DEV = 0.6745;
            end
            if ismember('HUBER_THRESH', evalin('base', 'who'))
                obj.HUBER_THRESH = evalin('base', 'HUBER_THRESH');
            else
                obj.HUBER_THRESH = 1.345;
            end
            if ismember('HUBER_DEV', evalin('base', 'who'))
                obj.HUBER_DEV = evalin('base', 'HUBER_DEV');
            else
                obj.HUBER_DEV = 0.6745;
            end
            if ismember('ALGORITHM', evalin('base', 'who'))
                obj.ALGORITHM = evalin('base', 'ALGORITHM');
            else
                obj.ALGORITHM = NLOPT_LN_BOBYQA;
            end
            if ismember('ANG_BOUND', evalin('base', 'who'))
                obj.ANG_BOUND = evalin('base', 'ANG_BOUND');
            else
                obj.ANG_BOUND = 90;
            end
            if ismember('ANG_STEP', evalin('base', 'who'))
                obj.ANG_STEP = evalin('base', 'ANG_STEP');
            else
                obj.ANG_STEP = 8;
            end
            if ismember('TRANS_STEP', evalin('base', 'who'))
                obj.TRANS_STEP = evalin('base', 'TRANS_STEP');
            else
                obj.TRANS_STEP = 15;
            end
            if ismember('MAX_IT', evalin('base', 'who'))
                obj.MAX_IT = evalin('base', 'MAX_IT');
            else
                obj.MAX_IT = 100;
            end
            if ismember('INIT_PLANE', evalin('base', 'who'))
                obj.initArgs = obj.initArgs+evalin('base', 'INIT_PLANE');
            end
        end
        
        function add_fullres(obj, path)
            obj.fullRes = Symm(path);
        end
        
        function crop_vol_z(obj, zMin, zMax)
            obj.vOrig = obj.vOrig(:, :, zMin:zMax);
            
            obj.maskBounds = int16([size(obj.vOrig, 1)/4, 3*size(obj.vOrig, 1)/4;
                              5*size(obj.vOrig, 2)/16, 11*size(obj.vOrig, 2)/16;
                              1*size(obj.vOrig, 3)/16, 7*size(obj.vOrig, 3)/16;]);
            obj.make_mask();
        end
        
        function def_mask(obj, xMin, xMax, yMin, yMax, zMin, zMax)
            obj.maskBounds = [xMin, xMax; yMin, yMax; zMin, zMax;];
            
            obj.make_mask();
        end
        
        function write(obj, fname)
            vNew = uint16(obj.vOut);%rig); % change later
            
            mkdir(fname);
            info = obj.vInfo;
            info.ImagePositionPatient = obj.vImPos;
            for i = 1:size(vNew, 3)
                dicomwrite(vNew(:, :, i), [fname, '\', num2str(i)], info, 'CreateMode', 'copy');
                info.ImagePositionPatient(1, 3) = info.ImagePositionPatient(1, 3)+obj.vPxSpac(3);
            end
        end
        
        function enum_voxels(obj)
            [X, Y, Z] = meshgrid(1:size(obj.vOrig, 1), 1:size(obj.vOrig, 2), 1:size(obj.vOrig, 3));
            enum = [X(:), Y(:), Z(:)]';
            enum = [enum; ones(1, size(enum, 2))];
            obj.voxelMap = enum;
        end
        
        function enum_chunks(obj, dim, stride)
            offset = (stride-1)/2+1;
            [X, Y, Z] = meshgrid(1:size(obj.vOrig, 1), 1:size(obj.vOrig, 2), 1:size(obj.vOrig, 3));
            X = X(:);
            Y = Y(:);
            Z = Z(:);
            enum = [X(offset:stride:end), Y(offset:stride:end), Z(offset:stride:end)]';
            enum = [enum; ones(1, size(enum, 2))];
            obj.chunkMap = enum;
            obj.chunkDim = dim;
        end
        
        function make_mask(obj, varargin)
            obj.mask = false(size(obj.vOrig));
            if nargin>=2; obj.maskBounds = varargin{1}; end
            obj.mask(obj.maskBounds(1, 1)+1:obj.maskBounds(1, 2), ...
                        obj.maskBounds(2, 1)+1:obj.maskBounds(2, 2), ...
                        obj.maskBounds(3, 1)+1:obj.maskBounds(3, 2)) = true(obj.maskBounds(:, 2)'-obj.maskBounds(:, 1)');
        end
        
        function vol = masked(obj)
            eVal = min(min(min(obj.vOrig)));
            vol = obj.vOrig.*obj.mask+eVal*(~obj.mask);
        end
        
        function res = ncc(obj, R, t)
            leftMask = Symm.make_xform_mask(obj.voxelMap, R, t);
            
            leftVoxels = obj.voxelMap(:, leftMask);
            
            leftVals = interp3(obj.vOrig, leftVoxels(2, :), leftVoxels(1, :), leftVoxels(3, :));
            %trim = find((leftVals>MAX_THRESH)|(leftVals<MIN_THRESH));
            %leftVoxels(:, trim) = [];
            %leftVals(trim) = [];
            
            rightVoxels = Symm.make_T(R, t)*leftVoxels;
            rightVals = interp3(obj.vOrig, rightVoxels(2, :), rightVoxels(1, :), rightVoxels(3, :));
            %trim = find((rightVals>MAX_THRESH)|(rightVals<MIN_THRESH));
            %rightVals(trim) = [];
            %leftVals(trim) = [];
            
            %
%             vals = zeros(size(obj.vOrig));
%             for i = 1:size(leftVoxels, 2)
%                vals(leftVoxels(1, i), leftVoxels(2, i), leftVoxels(3, i)) = obj.vOrig(leftVoxels(1, i), leftVoxels(2, i), leftVoxels(3, i));
%             end
%             volumeViewer(vals);
%             
%             vals = zeros(size(obj.vOrig));
%             rightVoxels(1, :) = min(rightVoxels(1, :), size(obj.vOrig, 1));
%             rightVoxels(2, :) = min(rightVoxels(2, :), size(obj.vOrig, 2));
%             rightVoxels(3, :) = min(rightVoxels(3, :), size(obj.vOrig, 3));
%             rightVoxels = int16(max(rightVoxels, 1));
%             for i = 1:size(leftVoxels, 2)
%                vals(rightVoxels(1, i), rightVoxels(2, i), rightVoxels(3, i)) = obj.vOrig(rightVoxels(1, i), rightVoxels(2, i), rightVoxels(3, i));
%             end
%             volumeViewer(vals);


%             vals = zeros(size(obj.vOrig));
%             rightVoxels(1, :) = min(rightVoxels(1, :), size(obj.vOrig, 1));
%             rightVoxels(2, :) = min(rightVoxels(2, :), size(obj.vOrig, 2));
%             rightVoxels(3, :) = min(rightVoxels(3, :), size(obj.vOrig, 3));
%             rightVoxels = int16(max(rightVoxels, 1));
%             for i = 1:size(leftVoxels, 2)
%                vals(leftVoxels(1, i), leftVoxels(2, i), leftVoxels(3, i)) = obj.vOrig(leftVoxels(1, i), leftVoxels(2, i), leftVoxels(3, i));
%                vals(rightVoxels(1, i), rightVoxels(2, i), rightVoxels(3, i)) = obj.vOrig(rightVoxels(1, i), rightVoxels(2, i), rightVoxels(3, i));
%             end
%             volumeViewer(obj.add_plane_Rt(vals, R, t));
            %
            
            %leftVals = obj.vOrig(leftMask);%obj.vOrigMasked(leftMask);
            
            %leftVals = interp3(obj.vOrig, leftVoxels(2, :), leftVoxels(1, :), leftVoxels(3, :));
            %rightVals = interp3(obj.vOrig, rightVoxels(2, :), rightVoxels(1, :), rightVoxels(3, :));
            
            %rightVals = zeros(1, size(rightVoxels, 2));
            %for i = 1:size(rightVoxels, 2)
            %    leftVals(i) = obj.vOrig(leftVoxels(1, i), leftVoxels(2, i), leftVoxels(3, i));
            %    rightVals(i) = obj.vOrig(rightVoxels(1, i), rightVoxels(2, i), rightVoxels(3, i));
            %end
            
            %preVals = rightVals;
            leftVals(isnan(leftVals)) = min(min(min(obj.vOrig)));
            rightVals(isnan(rightVals)) = min(min(min(obj.vOrig)));
            
            l = leftVals;%-mean(leftVals);
            r = rightVals;%-mean(rightVals);
            acc = sum(r.*l);%l*r';
            var = sqrt(sum(l.^2)*sum(r.^2));
            if var==0
                res = 1000000;
            else
                res = -acc/(var);
            end
            
            
             px = histcounts(leftVals, obj.threshLo:obj.threshHi);
              py = histcounts(rightVals, obj.threshLo:obj.threshHi);
%              px = px/sum(px);
%              py = py/sum(py);
             hold off;
%              h = plot(obj.threshLo:obj.threshHi-1, smooth(px), 'Color', [30, 30, 200]/255, 'LineWidth', 2, 'UserData', 'Right Partition');
%              hold on;
%              plot(obj.threshLo:obj.threshHi-1, smooth(py), 'Color', [200, 30, 30]/255, 'LineWidth', 2, 'UserData', 'Left Partition');
%              title('Bone Density Distribution');
%              xlabel('Voxel Intensity');
%              ylabel('Number of Voxels');
%              legend;
%              drawnow;
%              saveas(h, sprintf('hist_%d.png', size(obj.corrs, 2)+1));
        end
        
        function res = chunk_ncc(obj, R, t)
            leftMask = Symm.make_xform_mask(obj.chunkMap, R, t);
            
            leftVoxels = obj.chunkMap(:, leftMask);
            rightVoxels = Symm.make_T(R, t)*leftVoxels;
            
            shift = repmat(-(obj.chunkDim-1)/2:(obj.chunkDim-1)/2, [1, obj.chunkDim^2*size(leftVoxels, 2)]);
            shift = [shift; repmat(kron(-(obj.chunkDim-1)/2:(obj.chunkDim-1)/2, ones(1, obj.chunkDim)), [1, obj.chunkDim*size(leftVoxels, 2)])];
            shift = [shift; repmat(kron(-(obj.chunkDim-1)/2:(obj.chunkDim-1)/2, ones(1, obj.chunkDim^2)), [1, size(leftVoxels, 2)])];
            
            leftVoxels = kron(leftVoxels(1:3, :), ones(1, obj.chunkDim^3));
            leftVoxels = leftVoxels+shift;
            rightVoxels(1:3, :) = kron(rightVoxels(1:3, :), ones(1, obj.chunkDim^3));
            rightVoxels = rightVoxels+shift;
            
            leftVals = interp3(obj.vOrig, leftVoxels(2, :), leftVoxels(1, :), leftVoxels(3, :));
            leftVals(isnan(leftVals)) = min(min(min(obj.vOrig)));
            rightVals = interp3(obj.vOrig, rightVoxels(2, :), rightVoxels(1, :), rightVoxels(3, :));
            rightVals(isnan(rightVals)) = min(min(min(obj.vOrig)));
            
            l = leftVals;
            r = rightVals;
            acc = sum(r.*l);
            var = sqrt(sum(l.^2)*sum(r.^2));
            if var==0
                res = 1000000;
            else
                res = -acc/(var);
            end
        end
        
        function res = huber(obj, R, t)
            leftMask = Symm.make_xform_mask(obj.voxelMap, R, t);
            rightMask = ~leftMask;
            if sum(leftMask)<sum(rightMask)
                leftVoxels = obj.voxelMap(:, leftMask);
            else
                leftVoxels = obj.voxelMap(:, rightMask);
            end
            rightVoxels = Symm.make_T(R, t)\leftVoxels;
            
            leftVals = obj.vOrig(leftMask);
            rightVals = interp3(obj.vOrig, rightVoxels(2, :), rightVoxels(1, :), rightVoxels(3, :));
            rightVals(isnan(rightVals)) = min(min(min(obj.vOrig)));
                        
            diff = abs(leftVals-rightVals);
            k = obj.HUBER_THRESH*median(diff)/obj.HUBER_DEV;
            valid = diff>k;
            res = sum(diff(valid).^2)+sum(k*diff(~valid));
        end
        
        function res = tukey(obj, R, t)
            leftMask = Symm.make_xform_mask(obj.voxelMap, R, t);
%            leftMask = ~leftMask;
%             leftMask = leftMask&obj.excludeMask&obj.includeMask;
             rightMask = ~leftMask;
%             rightMask = rightMask&obj.excludeMask&obj.includeMask;
             if sum(leftMask)<sum(rightMask)
                 leftVoxels = obj.voxelMap(:, leftMask);
             else
                 leftVoxels = obj.voxelMap(:, rightMask);
             end
             rightVoxels = Symm.make_T(R, t)*leftVoxels;
%             
%             trim = false(1, length(rightVoxels));
%             for it = 1:length(obj.excludeNs)
%                 trim = trim|(obj.excludeNs(:, it)'*((rightVoxels(1:3, :)-obj.excludeCs(:, it))>0));
%             end
%             for it = 1:length(obj.includeNs)
%                 trim = trim|(obj.includeNs(:, it)'*((rightVoxels(1:3, :)-obj.includeCs(:, it))<0));
%             end
%             leftVoxels(:, trim) = [];
%             rightVoxels(:, trim) = [];
            
            leftVals = interp3(obj.vOrig, leftVoxels(2, :), leftVoxels(1, :), leftVoxels(3, :));
            rightVals = interp3(obj.vOrig, rightVoxels(2, :), rightVoxels(1, :), rightVoxels(3, :));
            rightVals(isnan(rightVals)) = min(min(min(obj.vOrig)));
                        
            diff = abs(leftVals-rightVals);
            %k = obj.TUKEY_THRESH*median(diff)/obj.TUKEY_DEV;
            %valid = diff<=k;
            %res = sum(diff(valid).^2.*(1-diff(valid)/k.^2).^2)/(sum(valid)^2);%sum(diff(valid).^2.*(1-diff(valid).^2).^2);
            k = median(diff)/obj.TUKEY_DEV;
            valid = diff/k<=obj.TUKEY_THRESH;
            res = sum(diff(valid).^2.*(1-diff(valid)/obj.TUKEY_THRESH.^2).^2)/(sum(valid)^2);%sum(diff(valid).^2.*(1-diff(valid).^2).^2);
            
            if res<obj.minScore
                obj.minScore = res;
                obj.minLeftVoxels = leftVoxels;
                obj.minRightVoxels = rightVoxels;
                obj.minLeftVals = leftVals;
                obj.minRightVals = rightVals;
                obj.minValid = valid;
            end
            
            %
%             vol = zeros(size(obj.vOrig));
%             for it = 1:length(valid)
%                if valid(it) && obj.vOrig(leftVoxels(1, it), leftVoxels(2, it), leftVoxels(3, it))>1200
%                    vol(leftVoxels(1, it), leftVoxels(2, it), leftVoxels(3, it)) = 1;
%                end
%             end
%             volumeViewer(vol);
% %             
%             vol = zeros(size(obj.vOrig));
%             for it = 1:length(valid)
%                if ~valid(it) && obj.vOrig(leftVoxels(1, it), leftVoxels(2, it), leftVoxels(3, it))>1200
%                    vol(leftVoxels(1, it), leftVoxels(2, it), leftVoxels(3, it)) = 1;
%                end
%             end
%             volumeViewer(vol);
        end
        
        function res = tukeyMI(obj, R, t)
             leftMask = Symm.make_xform_mask(obj.voxelMap, R, t);
             rightMask = ~leftMask;
             if sum(leftMask)<sum(rightMask)
                 leftVoxels = obj.voxelMap(:, leftMask);
             else
                 leftVoxels = obj.voxelMap(:, rightMask);
             end
             rightVoxels = Symm.make_T(R, t)*leftVoxels;
             
            leftVals = interp3(obj.vOrig, leftVoxels(2, :), leftVoxels(1, :), leftVoxels(3, :));
            rightVals = interp3(obj.vOrig, rightVoxels(2, :), rightVoxels(1, :), rightVoxels(3, :));
            rightVals(isnan(rightVals)) = min(min(min(obj.vOrig)));
                        
            diff = abs(leftVals-rightVals);
            k = median(diff)/obj.TUKEY_DEV;
            valid = diff/k<=obj.TUKEY_THRESH;
            res = sum(diff(valid).^2.*(1-diff(valid)/obj.TUKEY_THRESH.^2).^2)/(sum(valid)^2);
            
            if res<obj.minScore
                obj.minScore = res;
                obj.minLeftVoxels = leftVoxels;
                obj.minRightVoxels = rightVoxels;
                obj.minLeftVals = leftVals;
                obj.minRightVals = rightVals;
                obj.minValid = valid;
            end
            
            %if mod(length(leftVals), 2); leftVals = leftVals(1:end-1); end
            %if mod(length(rightVals), 2); rightVals = rightVals(1:end-1); end
            %L = reshape(leftVals, [2, length(leftVals)/2]);
            %R = reshape(rightVals, [2, length(rightVals)/2]);
            %res = -mi(L, R);
            leftVals = round(leftVals);
            rightVals = round(rightVals);
            range = find(leftVals>obj.threshLo&leftVals<obj.threshHi&rightVals>obj.threshLo&rightVals<obj.threshHi);
            pxy = zeros(obj.threshHi-obj.threshLo, obj.threshHi-obj.threshLo);
            for it = 1:length(range)
                pxy(leftVals(range(it))-obj.threshLo, rightVals(range(it))-obj.threshLo) = pxy(leftVals(range(it))-obj.threshLo, rightVals(range(it))-obj.threshLo)+1;
            end
            
             px = histcounts(leftVals, obj.threshLo:obj.threshHi);
             py = histcounts(rightVals, obj.threshLo:obj.threshHi);
%             px = px/sum(px);
%             py = py/sum(py);
             
               hold off;
%               h = figure('pos', [0, 0, 3012-1083, 787-283]);%1351
               plot(obj.threshLo:obj.threshHi-1, px, 'Color', [30, 30, 200]/255, 'LineWidth', 2, 'UserData', 'Right Partition');
               hold on;
               plot(obj.threshLo:obj.threshHi-1, py, 'Color', [200, 30, 30]/255, 'LineWidth', 2, 'UserData', 'Left Partition');
               xlim([obj.threshLo, obj.threshHi]);
%               title(sprintf('Bone Density Distribution (Iteration %d)', size(obj.corrs, 2)+1), 'FontSize', 42);
%               xlabel('Voxel Intensity', 'FontSize', 24);
%               ylabel('Number of Voxels', 'FontSize', 24);
%               lgd = legend('Right Partition', 'Left Partition');
%               lgd.FontSize = 24;
               drawnow;
%               saveas(h, sprintf('hist_%d.png', size(obj.corrs, 2)+1));
               
               

             px = px/sum(px);
             py = py/sum(py);

%            
             pxpy = px'*py;
             mi = sum(pxy(pxy>0&pxpy>0).*log2(pxy(pxy>0&pxpy>0)./pxpy(pxy>0&pxpy>0)))/length(range);
             %mi = mi/length(range);
%             
             %plot(smooth(px), 'LineWidth', 2);
             %hold on;
             %plot(smooth(py));
             %hold off;
             %drawnow;
%             
             LAMBDA1 = 1;
             LAMBDA2 = -0.5;%-0.5; %-.1
%             obj.debugTrace = [obj.debugTrace; [exp(LAMBDA1*res), LAMBDA2*mi]];
             fprintf('%f    %f\n', res, mi);
             %res = LAMBDA1*res+LAMBDA2*mi;
             res = LAMBDA1*res+LAMBDA2*mi;
            
%            range = obj.threshLo:obj.threshHi;
%            lCounts = histcounts(leftVals, range);
%            rCounts = histcounts(rightVals, range);
%             lCounts = smooth(lCounts, .04);
%             rCounts = smooth(rCounts, .04);
%             
%             %norm = 1;%max(sum(lCounts), sum(rCounts));
%             lCounts = lCounts/log2(sum(lCounts)); % normalize?
%             rCounts = rCounts/log2(sum(rCounts));
%             
%             range = range(1:end-1); % display
%             plot(range, lCounts);
%             hold on;
%             plot(range, rCounts);
%             hold off;
%             drawnow;
%             
%             d1 = log2(lCounts)-log2(rCounts);
%             d2 = log2(rCounts)-log2(lCounts);
%             d1(d1==Inf|d1==-Inf) = 0;
%             d1(isnan(d1)) = 0;
%             d2(d2==Inf|d2==-Inf) = 0;
%             d2(isnan(d2)) = 0;
%             kl1 = sum(lCounts.*d1);
%             kl2 = sum(rCounts.*d2);
%             
%             fprintf('%f : %f\n', kl1, kl2);
%             res = (kl1+kl2)/2;%(kl1+kl2)/2;%2*res+(kl1+kl2)/2;
        end
        
        function res = eval_wrap(obj, args)
            [R, t] = Symm.make_Rt(args);
            if strcmp('ncc', obj.ERR_METRIC)
                res = obj.ncc(R, t);
            elseif strcmp('tukey', obj.ERR_METRIC)
                res = obj.tukey(R, t);
            elseif strcmp('tukeyMI', obj.ERR_METRIC)
                res = obj.tukeyMI(R, t);
            elseif strcmp('huber', obj.ERR_METRIC)
                res = obj.huber(R, t);
            elseif strcmp('chunk_ncc', obj.ERR_METRIC)
                res = obj.chunk_ncc(R, t);
            end
            obj.corrs = [obj.corrs, res];
            fprintf('%d: %f\n', size(obj.corrs, 2), res);

%              obj.fullRes.vOut = obj.fullRes.add_plane_Rt(obj.fullRes.vOrig, R, 2*t);
%              obj.fullRes.write(sprintf('plane_%d', size(obj.corrs, 2)));
%              map = Symm.make_xform_mask(obj.fullRes.voxelMap, R, 2*t);
%              map = reshape(map, [size(obj.fullRes.vOrig, 2), size(obj.fullRes.vOrig, 1), size(obj.fullRes.vOrig, 3)]);
%              map = permute(map, [2, 1, 3]);
%              vol = obj.fullRes.mirror_vol(R, 2*t);
%              vol(map) = 6000+vol(map);
%              obj.fullRes.vOut = vol;
%              obj.fullRes.write(sprintf('mirr_%d', size(obj.corrs, 2)));
        end
        
        function corr = find_symm(obj)
            obj.corrs = [];
            obj.debugTrace = [];
            obj.minScore = inf;
            
            opt.algorithm = obj.ALGORITHM;
            opt.min_objective = @obj.eval_wrap;
            opt.lower_bounds = [-obj.ANG_BOUND, -obj.ANG_BOUND, -obj.ANG_BOUND, double(obj.maskBounds(:, 1)')];
            opt.upper_bounds = [obj.ANG_BOUND, obj.ANG_BOUND, obj.ANG_BOUND, double(obj.maskBounds(1, 2)), double(obj.maskBounds(2, 2)), double(obj.maskBounds(3, 2))];
            opt.maxeval = obj.MAX_IT;
            opt.initial_step = [obj.ANG_STEP, obj.ANG_STEP, obj.ANG_STEP, obj.TRANS_STEP, obj.TRANS_STEP, obj.TRANS_STEP];
            [obj.optArgs, corr, retCode] = nlopt_optimize(opt, obj.initArgs);
            retCode
            
            %
            %options = optimoptions(@fmincon, 'MaxIterations', 50); %50?
            %[obj.optArgs, corr] = fmincon(@obj.eval_wrap, obj.initArgs, [], [], [], [], [-45, -45, -45, double(obj.maskBounds(:, 1)')], [45, 45, 45, double(obj.maskBounds(:, 2)')], [], options);
        end
        
        function newVol = add_plane(obj, vol, args)
            newVol = vol;
            [R, t] = Symm.make_Rt(args);
            n = R*[0;1;0];
            mask = abs(n'*(obj.voxelMap(1:3, :)-t))<=1;%.5;
            mask = reshape(mask, [size(vol, 2), size(vol, 1), size(vol, 3)]);
            mask = permute(mask, [2,1,3]);
            newVol(mask(:)') = 7000;%max(max(max(newVol)));
        end
        
        function newVol = add_plane_Rt(obj, vol, R, t)
            newVol = vol;
            n = R*[0;1;0];
            mask = abs(n'*(obj.voxelMap(1:3, :)-t))<=.5;
            
            mask = reshape(mask, [size(vol, 2), size(vol, 1), size(vol, 3)]);
            mask = permute(mask, [2,1,3]);
            
            newVol(mask(:)') = 5000;%max(max(max(newVol)));
        end
        
        function mirror(obj)
            [R, t] = Symm.make_Rt(obj.optArgs);
            obj.vOut = obj.mirror_vol(R, t);
        end
        
        function orig_display(obj)
            volumeViewer(obj.add_plane(obj.vOrig, obj.initArgs));
        end
        
        function new_display(obj)
            %obj.mirror();
            volumeViewer(obj.add_plane(obj.vOrig, obj.optArgs));
        end
        
        function debug_display(obj, R, t)
            volumeViewer(obj.add_plane_Rt(obj.vOrig, R, t));
        end
        
        function v_new = mirror_vol(obj, R, t)
            fprintf('mirroring... ');
            voxelMap = obj.voxelMap;
    
            mask = Symm.make_xform_mask(obj.voxelMap, R, t);
    
            voxelMap(:, mask) = Symm.make_T(R, t)*obj.voxelMap(:, mask);%repmat([-1;-1;-1;-1], 1, sum(mask));
    
            new_vals = interp3(double(obj.vOrig), voxelMap(2, :), voxelMap(1, :), voxelMap(3, :));
            new_vals(isnan(new_vals)) = min(min(min(obj.vOrig)));
            v_new = reshape(new_vals, [size(obj.vOrig, 2), size(obj.vOrig, 1), size(obj.vOrig, 3)]);
            v_new = permute(v_new, [2, 1, 3]);
            %?% v_new = reshape(v_new, [2,1,3]);
        
            fprintf('done\n');
        end
        
        function [selection, Ns, Cs] = select(obj, pts)
            intPt = mean(pts, 2);
            intDir = intPt-pts;
            A = zeros(8);
            for it = 1:8
                dirs = pts-pts(:, it);
                [~, inds] = mink(dot(dirs, repmat(intDir(:, it), [1, 8]))./(sqrt(sum(dirs.^2))), 4);
                A(it, inds(1)) = 1;
                A(it, inds(2)) = 1;
                A(it, inds(4)) = 1;
            end
            
            if ~all(sum(A)==3); fprintf('convexity error...?\n'); end
            
            %dot(pts, intDir)./(sqrt(sum(intDir.^2)).*);
            
            %ps = combnk(1:8, 3);
            B = A^2;
            ps = [[ones(3, 1); find(B(2:end, 1))+1], repmat(combnk(find(A(:, 1)), 2), [2, 1])];
            opp = find(A+B==0);
            opp = opp(1);
            finds = find(B(:, opp));
            finds(finds==opp) = [];
            ps = [ps; [opp*ones(3, 1); finds], repmat(combnk(find(A(:, opp)), 2), [2, 1])];
            selection = true(1, length(obj.vOrig(:)));
            Ns = [];
            Cs = [];
            for it = 1:length(ps)
                a = pts(:, ps(it, 1));
                b = pts(:, ps(it, 2));
                c = pts(:, ps(it, 3));
                n = cross(c-a, c-b);
                n = n/norm(n);
                if n'*(intPt-c)>0; n = -1*n; end
                Ns = [Ns, n];
                Cs = [Cs, c];
                
                %vals = n'*(obj.voxelMap(1:3, :)-c)>0;
                
                selection(n'*(obj.voxelMap(1:3, :)-c)>0) = false;
            end
            
            %selection = reshape(selection, [size(obj.vOrig, 2), size(obj.vOrig, 1), size(obj.vOrig, 3)]);
            %selection = permute(selection, [2,1,3]);
        end
    
        function dislocate(obj, pts, R, t)
            destPts = R*pts+t;
            [oSelection, ~, ~] = obj.select(pts);
            oSelection = reshape(oSelection, [size(obj.vOrig, 2), size(obj.vOrig, 1), size(obj.vOrig, 3)]);
            oSelection = permute(oSelection, [2,1,3]);
            [nSelection, ~, ~] = obj.select(destPts);
            nVoxels = obj.voxelMap(:, nSelection);
            oVoxels = [R', -R'*t; 0, 0, 0, 1]*nVoxels;
            vals = interp3(obj.vOrig, oVoxels(2, :), oVoxels(1, :), oVoxels(3, :));
            vals(isnan(vals)) = min(min(min(obj.vOrig)));
            obj.vOut = obj.vOrig;
            
            obj.vOut(oSelection) = min(min(min(obj.vOrig)));
            for it = 1:length(nVoxels)
                obj.vOut(nVoxels(1, it), nVoxels(2, it), nVoxels(3, it)) = vals(it);
            end
        end
        
        function exclude(obj, bounds)
            if isempty(bounds); return; end
            [obj.excludeMask, obj.excludeNs, obj.excludeCs] = obj.select(bounds);
            obj.excludeMask = ~obj.excludeMask;
        end
        
        function include(obj, bounds)
            if isempty(bounds); return; end
            [obj.includeMask, obj.includeNs, obj.includeCs] = obj.select(bounds);
            %obj.excludeMask = ~obj.excludeMask;
        end
        
        function vol = exclude_vol(obj)
            vol = obj.vOrig;
            mask = reshape(obj.excludeMask, [size(obj.vOrig, 2), size(obj.vOrig, 1), size(obj.vOrig, 3)]);
            mask = permute(mask, [2,1,3]);
            vol(mask) = min(min(min(obj.vOrig)));
        end
        
        function vol = include_vol(obj)
            vol = obj.vOrig;
            mask = reshape(obj.includeMask, [size(obj.vOrig, 2), size(obj.vOrig, 1), size(obj.vOrig, 3)]);
            mask = permute(mask, [2,1,3]);
            vol(~mask) = min(min(min(obj.vOrig)));
        end
        
        function disp_liers(obj)
            vol = zeros(size(obj.vOrig));
            for it = 1:length(obj.minValid)
                if obj.minValid(it) && obj.vOrig(obj.minLeftVoxels(1, it), obj.minLeftVoxels(2, it), obj.minLeftVoxels(3, it))>1200
                    vol(obj.minLeftVoxels(1, it), obj.minLeftVoxels(2, it), obj.minLeftVoxels(3, it)) = 1;
                end
            end
            volumeViewer(vol);
             
            vol = zeros(size(obj.vOrig));
            for it = 1:length(obj.minValid)
               if ~obj.minValid(it) && obj.vOrig(obj.minLeftVoxels(1, it), obj.minLeftVoxels(2, it), obj.minLeftVoxels(3, it))>1200
                   vol(obj.minLeftVoxels(1, it), obj.minLeftVoxels(2, it), obj.minLeftVoxels(3, it)) = 1;
               end
            end
            volumeViewer(vol);
        end
        
        function proc_hist(obj)
            lo = min(obj.vOrig(:));
            hi = max(obj.vOrig(:));
            
            [counts, bins] = histcounts(obj.vOrig(:), lo:hi);
            bins = bins(1:end-1);
            [pks, inds] = findpeaks(-smooth(smooth(smooth(counts))));
            [~, ind] = min(pks);
            ind = inds(ind+1);
            obj.threshLo = bins(ind);
            bins = bins(ind:end);
            counts = counts(ind:end);
            found = find(counts==0);
            obj.threshHi = bins(found(1)); % ???
            
            %obj.vOrig(obj.vOrig<obj.threshLo|obj.vOrig>obj.threshHi) = obj.threshLo;
            %vol = obj.vOrig;
            %vol(vol<obj.threshLo|vol>obj.threshHi) = obj.threshLo;
            %volumeViewer(vol);
        end
    end
    methods(Static)        
        function T = make_T(R, t)
            TToWorld = [R, t; 0, 0, 0, 1];
            invT = [R', -R'*t; 0, 0, 0, 1];
            M = eye(4);
            M(2, 2) = -1;
            T = TToWorld*M*invT;%TToWorld\M*TToWorld;
        end
        
        function [R, t] = make_Rt(args)
            %x = sind(args(1))*cosd(args(2));
            %y = sind(args(1))*sind(args(2));
            %z = cosd(args(1));
            R = eul2rotm(deg2rad([args(3), args(2), args(1)]));
            %R = axang2rotm([x, y, z, args(3)]);
            t = [args(4); args(5); args(6)];
        end
        
        function mask = make_xform_mask(map, R, t)
            n = R*[0;1;0];
            mask = n'*(map(1:3, :)-t)<0;
            %mask = reshape(mask, [size(obj.vOrig, 2), size(obj.vOrig, 1), size(obj.vOrig, 3)]);
            %mask = permute(mask, [2,1,3]);
            %mask = mask(:);
            
            %vals = reshape(n'*(map(1:3, :)-t), [125,200,69]);
        end
    end
end

% '61.7.167248355135476067044532759811631626828/'
            
            %xcorr(leftVals, rightVals, 'coeff')
            %res = max(xcorr(leftVals, rightVals, 'coeff'));
            
            %leftVals = leftVals/max(leftVals);
            %rightVals = rightVals/max(rightVals);
            %res = mean((leftVals-rightVals).^2);
            
            %voxelMap = obj.voxelMap;
            %voxelMap(:, ~rightMask) = repmat([-1;-1;-1;-1], 1, sum(~rightMask));%T*voxelMap(:, mask);
            
            %new_vals = interp3(obj.vOrig, voxelMap(1, :), voxelMap(2, :), voxelMap(3, :));
            %new_vals(isnan(new_vals)) = min(min(min(obj.vOrig)));
            %vol = reshape(new_vals, size(obj.vOrig));
            %volumeViewer(vol);