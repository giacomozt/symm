% FILE PARAMS
VOLUME = 'D:\symm\data\DICOMs\75_cropped_smoothed';%'C:\Users\singc\Desktop\newDICOM';%'61.7.167248355135476067044532759811631626828';
VOL_OUT_NAME = 'mirrored';

% ERROR METRIC PARAMS
ERR_METRIC = 'tukey'; % 'ncc' / 'tukey' / 'huber'
TUKEY_THRESH = 4.685;
TUKEY_DEV = 0.6745;

% ALGORITHM PARAMS
ALGORITHM = NLOPT_LN_BOBYQA;
ANG_BOUND = 90; % +/-
ANG_STEP = 4;
TRANS_STEP = 6;
MAX_IT = 100;

% INITIALIZATION
INCLUDE_PTS = [];
EXCLUDE_PTS = [];
INIT_PLANE = [0, 0, 0, 0, 0, 0];
% initial plane params = [0, 0, 0, 0, 0, 0]; % default: zero angular, translational middle

% DISPLAY
DISP_ORIG = true;
DISP_NEW = true;
DISP_MIRR = false;
DISP_LIERS = true;


if (~exist('s'))
    s = Symm(VOLUME);
    s.include(INCLUDE_PTS);
    s.exclude(EXCLUDE_PTS);
end

if DISP_ORIG; s.orig_display(); end
s.find_symm();
if DISP_NEW; s.new_display(); end
s.mirror();
if DISP_MIRR; volumeViewer(s.vOut); end
s.write(VOL_OUT_NAME); % writes to relative folder, usually D:\symm

