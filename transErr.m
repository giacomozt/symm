function errs = transErr(args)
    errs = zeros(size(args));
    
    tTrue = [100; 50; 50];
    for y = 1:size(args, 1)
        for x = 1:size(args, 2)
            arg = args{y, x};
            t = [arg(4); arg(5); arg(6)];
            tDel = tTrue-t;
            R = eul2rotm(deg2rad([arg(3), arg(2), arg(1)]));
            n = R*[0;1;0];
            %tDel = (n'*tDel)*tDel;
            %errs(y, x) = norm(tDel);
            errs(y, x) = abs(n'*tDel);
        end
    end
end