load('data\synth4.mat');
s = Symm('D:\symm\data\DICOMs\75_cropped_smoothed');
s.vOrig = synth;
s.maskBounds = [0, 200; 0, 100; 0, 100];
s.make_mask();
s.enum_voxels();

nccVals = -1*ones(11);
nccArgs = num2cell(zeros(11));
tukeyVals = zeros(11);
tukeyArgs = num2cell(zeros(11));

nccPlot = surf(1+nccVals);
figure;
tukeyPlot = surf(tukeyVals);
drawnow;

for x = 0:5
    s.vOrig = synth;
    %s.vOrig(35:35+3*x, 60:60+3*x, 35:35+3*x) = 300;
    s.vOrig(50-4*x:50+4*x, 75-1.5*x:75+1.5*x, 50-4*x:50+4*x) = 300;
    for y = 0:5
        s.initArgs = [0, 0, 0, 100, 50, 50] + .2*y*[-10, 0, 10, 10, -5, 5];
        s.ERR_METRIC = 'ncc';
        fprintf('running NCC at %d%% anomaly size, %d%% displacement\n', 10*x, 10*y);
        nccVals(x+1, y+1) = s.find_symm();
        nccArgs{x+1, y+1} = s.optArgs;
        
        nccPlot.set('ZData', 1+nccVals);
        %nccPlot = surf(1+nccVals);
        drawnow;
        
        s.ERR_METRIC = 'tukey';
        fprintf('running tukey at %d%% anomaly size, %d%% displacement\n', 10*x, 10*y);
        tukeyVals(x+1, y+1) = s.find_symm();
        tukeyArgs{x+1, y+1} = s.optArgs;
        
        tukeyPlot.set('ZData', tukeyVals);
        %tukeyPlot = surf(tukeyVals);
        drawnow;
    end
end